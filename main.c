#include<stdio.h>

typedef 
int (*vmethod) (void*);

vmethod *vtable_matrix_class;// = malloc(sizeof(vmethod));
vmethod *vtable_vector_class;// = malloc(sizeof(vmethod));

typedef struct Matrix_ {
	int rows;
	int columns;
	int *array;

	void* (*add)(void*, void*);
	void* (*mult)(void*, void*);

	vmethod *vtable_ptr;

} Matrix;

typedef struct Vector_ {
	Matrix inherited;
	int min;
	int max;

	vmethod *vtable_ptr;

} Vector;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rand_matrix(Matrix* this)
{
	int i=0, j=0;

	for (i=0; i<this->rows; i++)
		for (j=0; j<this->columns; j++)
			this->array[i*this->columns + j] = i*this->columns + j + 1;
}

Matrix* addMat (Matrix* this, Matrix* B) 
{
	Matrix *result = malloc(sizeof(Matrix));
	
	if (this->columns == B->columns && this->rows == B->rows)
	{
		
		init_matrix(result, this->rows, this->columns);

		int i,j;

		for (i=0; i<this->rows; i++)
			for (j=0; j<this->columns; j++)
			{
				int e = i*this->columns + j;
				result->array[e] = this->array[e] + B->array[e];
			}
	}

	else
		printf("\nAddition not Possible\n");

	return result;
}

Matrix* mult (Matrix* this, Matrix* B) 
{
	Matrix *result = malloc(sizeof(Matrix));
	
	if (this->columns == B->rows)
	{
		init_matrix(result, this->rows, B->columns);

		int i,j,k;

		for (i=0; i<this->rows; i++)
			for (j=0; j<B->columns; j++)
			{
				result->array[i*result->columns + j] = 0;
				for (k=0; k<this->columns; k++)
				{
					result->array[i*result->columns + j] += this->array[i*this->columns + k] * B->array[k*B->columns + j];
				}
			}
	}

	else
		printf("\nMultiplication not Possible\n");

	return result;
}

int l1NormMat(Matrix* this)
{
	int i,j,sum = 0;

	int max = this->array[0];

	for ( i=0; i<this->rows; i++)
	{
		sum = 0;
		for ( j=0; j<this->columns; j++)
			sum += this->array[j*this->columns + i];

		if (max < sum)
			max = sum;
	}

	return max;
}

void init_matrix(Matrix* this, int rows, int columns)
{
	if (rows < 1 || columns < 1)
	{
		printf("Number of rows/columns cannot be less than 1\n");
		exit(0);
	}

	this->rows = rows;
	this->columns = columns;
	this->array = malloc(sizeof(int)*rows*columns);

	this->add = &addMat;
	this->mult = &mult;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rand_vector(Vector *this)
{
	int j=0;

	for (j=0; j<this->inherited.rows; j++)
		this->inherited.array[j] = j + 1;
}

Vector* addVec (Vector* this, Vector* B) 
{
	Vector *result = malloc(sizeof(Vector));
	
	if (this->inherited.rows == B->inherited.rows)
	{
		
		init_vector(result, this->inherited.rows);

		int i,j;

		for (i=0; i<this->inherited.rows; i++)
				result->inherited.array[i] = this->inherited.array[i] + B->inherited.array[i];
	}

	else
		printf("\nAddition not Possible\n");

	return result;
}

int l1NormVec(Vector* this)
{
	int sum = 0, i=0;

	for (i =0; i<this->inherited.rows; i++)
		sum += this->inherited.array[i];

	return sum;
}

void init_vector(Vector* this, int rows)
{
	if (rows < 1)
	{
		printf("Number of rows cannot be less than 1\n");
		exit(0);
	}


	this->inherited.columns = 1;
	this->inherited.rows = rows;
	this->inherited.array = malloc(sizeof(int)*rows);

	this->min = 0;
	this->max = 9;

	this->inherited.add = &addVec;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void display(Matrix* this)
{
	int i, j;

	printf("\n");

	for (i=0; i<this->rows; i++)
	{
		for (j=0; j<this->columns; j++)
			printf("%d\t",this->array[i*this->columns + j]);
		printf("\n");
	}
}

int main (void)
{
	vtable_matrix_class = malloc(sizeof(vmethod));
	vtable_vector_class = malloc(sizeof(vmethod));

	vtable_matrix_class[0] = &l1NormMat;
	vtable_vector_class[0] = &l1NormVec;

	char opt = '3';
		
//	printf("Press 1 for matrix matrix multiplication and press 2 for matrix vector multiplication\n");

//	opt = getchar();

	if (opt == '1')
	{
		Matrix A;
		init_matrix(&A, 3, 3);
		rand_matrix(&A);
		Matrix B;
		init_matrix(&B, 3, 3);
		rand_matrix(&B);

		//display(&A);
		//display(&B);

		display(A.add(&A, &B));	
		display(A.mult(&A, &B));
	}

	else if (opt == '2')
	{
		Vector a;
		init_vector(&a,3);
		rand_vector(&a);
		Vector b;
		init_vector(&b,3);
		rand_vector(&b);

		display(a.inherited.add(&a, &b));
	}

	else if (opt == '3')
	{
		Matrix *m = (Matrix*)malloc(sizeof(Vector));
		init_matrix(m,3,1);
		m->vtable_ptr = vtable_vector_class;
		rand_matrix(m);

		Vector b;
		init_vector(&b,3);
		rand_vector(&b);
	
//		display(m);

		m->add(m, &b);				//statically bound, add function for matrix called

		int l1Norm = m->vtable_ptr[0](m);						//dynamically bound, l1Norm function for vector called
		//printf("\n%d\n",l1Norm);

		Matrix *M = malloc(sizeof(Matrix));
		init_matrix(M,3,3);
		M->vtable_ptr = vtable_matrix_class;
		rand_matrix(M);

//		display(M);

		l1Norm = M->vtable_ptr[0](M);						//dynamically bound, l1Norm function for matrix called
		//printf("\n%d\n",l1Norm);

	}

	else
		printf("Invalid choice\n");

	printf("done\n");

	return 0;
}
